This repository contains examples of common usage for GitLab CI with docker, docker buildKit experimental feature and Makefile (mainly for docker alias commands).

## Contributing

I am always open to new ideas. Feel free to create new MR's if you would like.

## References:

- [.gitlab-ci file reference](https://docs.gitlab.com/ee/ci/yaml/README.html);

- [Using Git submodules in your CI jobs](https://docs.gitlab.com/ee/ci/git_submodules.html#using-git-submodules-with-gitlab-ci);

- [docker buildx CLI plugin](https://github.com/docker/buildx);

- [aquasecurity/trivy](https://github.com/aquasecurity/trivy);

- [Trivy in GitLab CI](https://aquasecurity.github.io/trivy/latest/integrations/gitlab-ci/);

- [SSH keys when using the Docker executor](https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor);
    - [Example SSH Project](https://gitlab.com/gitlab-examples/ssh-private-key/)

- CI SSH exported variables are empty:
    - https://stackoverflow.com/questions/15778403/how-to-pass-local-variable-to-remote-using-ssh-and-bash-script
    - https://stackoverflow.com/questions/27932694/shell-script-ssh-server-eof
    - https://stackoverflow.com/questions/305035/how-to-use-ssh-to-run-a-local-shell-script-on-a-remote-machine
    - https://stackoverflow.com/questions/7114990/pseudo-terminal-will-not-be-allocated-because-stdin-is-not-a-terminal

## TODO

- automate docker secret creation - needs research;
- use vault with GitLab CI: https://docs.gitlab.com/ee/ci/secrets/index.html#use-vault-secrets-in-a-ci-job
    - https://stackoverflow.com/questions/42139605/how-do-you-manage-secret-values-with-docker-compose-v3-1