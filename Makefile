# Get Makefile full path
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
# Get Makefile dir name only
root_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

# Force to use buildkit for all images and for docker-compose to invoke
# Docker via CLI (otherwise buildkit isn't used for those images)
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

# HELP
# This will output the help for each task
.PHONY: help -

# Automated helper
help: ## Show this help information.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

-: ## 

# LOCAL / DEV - docker-compose commands

up: ## Start development enviroment.
	@docker-compose up \
		--detach

logs: ## Display logs from all containers with timestamp.
	@docker-compose logs \
		--timestamps

ps: ## List all containers from project.
	@docker-compose ps \
		--all

build: ## Build docker images from project for development.
	@docker-compose build

build-nc: ## Rebuild docker images (ignore existing cache).
	@docker-compose build \
		--no-cache

recreate: ## Force recreate all containers.
	@docker-compose up \
		--force-recreate \
		--detach

reload: build recreate ## Build and force recreate all containers (using cache).

reload-nc: build-nc recreate ## Build and force recreate all containers (ignore existing cache).

down: ## Stop and remove all containers.
	@docker-compose down \
		--remove-orphans

DOCKERFILE ?= docker/node/node.dockerfile
lint-dockerfile: ## Lint any supplied Dockerfile.
	@docker run \
		--pull always \
		--rm \
		--interactive \
		--volume "${PWD}/.hadolint.yml:/.hadolint.yaml:ro" \
		hadolint/hadolint:latest < ${DOCKERFILE}

-: ## 

# DEPLOY - docker-stack commands

stack: SHELL:=/bin/bash
stack: ## Deploy/update the stack.
	@docker stack deploy \
		--compose-file <(docker-compose \
			--file docker-compose.yml \
			--file docker-stack.yml config) \
		--with-registry-auth \
		$(root_dir)

stack-down: ## Remove docker stack.
	@docker stack rm $(root_dir)