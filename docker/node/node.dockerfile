FROM node:latest

# Metadata
# Runtime distribution variables for labels
ARG BUILD_DATE
ARG VCS_COMMIT_SHA
ARG VCS_URL
# OCI image specs
# More OCI Image Format Specification:
# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL org.opencontainers.image.authors="Someone beaultiful - emailFromHandsomePerson@greatness.com" \
        org.opencontainers.image.created=${BUILD_DATE} \
        org.opencontainers.image.description="This image contains nodejs and something else cool for a cool purpose." \
        org.opencontainers.image.revision=${VCS_COMMIT_SHA} \
        org.opencontainers.image.source=${VCS_URL}